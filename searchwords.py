#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''
import sys
import sortwords


# Función "search_word": realiza la búsqueda de la palabra. Recibe como parámetros una palabra (que es la palabra a
# buscar) y una lista de palabras ordenada, en la cual se busca la posición de la palabra. La función devuelve la
# posición donde se ha encontrado la palabra en la lista ordenada y, si no lo encuentra, levanta la excepción
# "Exception".
def search_word(word, words_list):

    is_found = True
    for w in words_list:
        if is_found:
            if sortwords.equal(w, word):
                text_index = words_list.index(w)
                return text_index
        else:
            raise Exception


#Función "main": programa principal. Se pasan como argumentos una palabra y una lista de palabras (que puede estar
# ordenada o no). Para ordenar la lista, se llama a la función "sort" del módulo "sortwords". Posteriormente, se llama
# a la función "search_word" para realizar la búsqueda y devolver la posición. Para mostrar la lista de palabras
# ordenada, se llama a la función "show" del módulo "sortwords" proporcionado. Finalmente, se imprime también la
# posición de la palabra.
# Nota: si se han pasado menos de 2 argumentos llama a sys.exit con el mensaje de error: "Se deben pasar al menos 2
# argumentos".
# Nota*: si se levanta la excepción "Exception", la captura y termina el programa llamando a sys.exit.
def main():

    word = str(sys.argv[1])
    words_list = (sys.argv[2:])

    try:
        len(sys.argv) >= 2
    except:
        sys.exit("Se deben pasar al menos 2 argumentos.")

    try:
        ordered_list = sortwords.sort(words_list)
        position = search_word(word, ordered_list)
        sortwords.show(ordered_list)
        print(position)
    except Exception:
        sys.exit("No se ha encontrado la palabra.")


if __name__ == '__main__':
    main()